CREATE DATABASE IF NOT EXISTS dentist_appointments;
USE dentist_appointments;
CREATE TABLE IF NOT EXISTS appointments (
    id INT AUTO_INCREMENT PRIMARY KEY NOT NULL,
    name VARCHAR(255) NOT NULL,
    email VARCHAR(255) NOT NULL,
    date DATE NOT NULL,
    message VARCHAR(255)

);