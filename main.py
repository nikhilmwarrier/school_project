import mysql.connector
from datetime import datetime, timedelta
from sys import stderr

# Connect to MySQL database
mydb = mysql.connector.connect(
  host="localhost",
  user="root",
  password="",
  database="dentist_appointments"
)

# Create a cursor object to interact with the database
cursor = mydb.cursor()

def display_appointments():
    # Fetch all records from the appointments table
    cursor.execute("SELECT * FROM appointments ORDER BY date DESC")
    appointments = cursor.fetchall()

    # Display the appointments
    print("\n------------------------")
    for appointment in appointments:
        # print(f"Id: {appointment[0]}")
        print(f"Name: {appointment[1]}")
        print(f"Email: {appointment[2]}")
        print(f"Date: {appointment[3]}")
        if appointment[4]:
            print(f"Message: {appointment[4]}")
        print("------------------------")

def book_appointment():
    # Prompt the user for their name, email, and date of appointment
    print("\n------------------------")
    name = input("Enter your name: ")
    email = input("Enter your email address: ")
    date = input("Enter the date of appointment (YYYY-MM-DD): ")
    Message = input("Enter a short message for the doctor (optional): ")
    print("------------------------\n")

    try:
        # Insert the appointment into the database
        sql = "INSERT INTO appointments (name, email, date, Message) VALUES (%s, %s, %s, %s)"
        values = (name, email, date, Message)
        cursor.execute(sql, values)
        mydb.commit()
    except:
        print("ERROR: Error while updating database. Please check your input and try again.", file=stderr)
        return

    print("Appointment booked successfully!\n")

def view_due_appointments():
    # Calculate the date six months ago
    six_months_ago = datetime.now() - timedelta(days=180)

    # Fetch appointments older than
    sql = "SELECT * FROM appointments WHERE date <= %s"
    values = (six_months_ago,)
    cursor.execute(sql, values)
    appointments = cursor.fetchall()

    # Display the due appointments
    print("\nThese appointments are older than six months. Consider notifying these patients for a new appointment.")
    print("------------------------")
    for appointment in appointments:
        print(f"Name: {appointment[0]}")
        print(f"Email: {appointment[1]}")
        print(f"Date: {appointment[2]}")
        print("------------------------")

# Main program flow
while True:
    print("\nWelcome to the Dentist Appointment Scheduler!")
    print("Please select an option:")
    print("1. Book an appointment")
    print("2. View all appointments")
    print("3. View due appointments (older than six months)")
    print("4: Exit")
    choice = input("Enter your choice: ")

    if choice == "1":
        book_appointment()
    elif choice == "2":
        display_appointments()
    elif choice == "3":
        view_due_appointments()
    elif choice == "4":
        break
    else:
        print("Invalid choice. Please try again.", file=stderr)

    print("\n")

print("Terminating program...")