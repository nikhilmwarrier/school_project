---
title: Appointment Management System for a Dentist's Office
fontsize: 14pt
geometry: a4paper
documentclass: extarticle
header-includes: 
	- \usepackage[usestackEOL]{stackengine}
	- \usepackage{graphicx}
	- \graphicspath{ {.} }
	- \usepackage{fvextra}
	- \DefineVerbatimEnvironment{Highlighting}{Verbatim}{breaklines,commandchars=\\\{\}}
---

\thispagestyle{empty}

\begin{center}

       \vspace{1.5cm}

       \textbf{Nikhil M Warrier}

       \vfill

       An investigatory project in Computer Science\\
       for the academic year 2023-24

       \vspace{0.8cm}

       \includegraphics[width=0.4\textwidth]{kvs}

       Class 12-B (Batch 1)\\
       Kendriya Vidyalaya RB Kottayam

\end{center}

\newpage
\thispagestyle{empty}

## Certificate

This is to certify that this investigatory project in chemistry, titled
"Appointment Management System for a Dentist's Office" has been completed by Nikhil M Warrier of
class 12-B. This work has been done under the guidance of Smt Suni Abraham, PGT Computer Science.

\vfill
Subject Teacher
\hfill
Principal
\hfill
External Examiner

\newpage

## Table of Contents

- Certificate
- Acknowledgements
- Introduction
- Objectives
- Problem Statement
- Software Development Life Cycle (SDLC)
- Software Testing
- Source Code
- Output and Demonstration
- System Requirements
- Software Used
- Credits and Bibliography

\newpage
\thispagestyle{empty}

## Acknowledgements

I would like to extend my deepest gratitude to Smt Suni Abraham (PGT
Computer Science) for giving me the opportunity to complete this project, and for
providing every help and support possible.

My gratitude also goes to my parents and classmates for their help and support.

\newpage

## Introduction

This program helps a user schedule appointments with their dentist, and lets the dentist view the appointments that are present in their schedule. It also logs the last appointment the user had, and shows the dentist a list of users whose appointments are due.

The program uses a rudimentary username-password login system to ensure basic security. Password hashing should be ideally implemented but that is beyond the scope of this simple demonstratory program.

## Objectives

The objective of this project is to demonstrate a software program that can be used to improve the efficiency of a real-world task by automating it. This is also a demonstration of industry-standard programming practices that set apart "good code" from mediocre unmaintable code written by a certain unfortunate class of so-called programmers.

An outline of the objectives is as follows:

- Reduce a real-world situation into a computer program in order to optimise it.
- Program using modern software tools.
- Apply appropriate design patterns based on the scale of the project.
- Write code following modern style guides with an emphasis on readability and maintainability.
- Demonstrate the effective working of an interconnected software stack in a single program.

\newpage

## Problem Statement

Scheduling is a critical component of dental practice operations, yet it can present unique challenges for dental professionals. Dentists must juggle many competing tasks to create a productive daily schedule, taking into account patient needs, emergency requests, and time-consuming procedures. Furthermore, ensuring that their schedule is full while avoiding excessive wait times and double booking can be a difficult balance to maintain. Additionally, scheduling around holidays and staff vacations can further complicate the process. To manage these scheduling demands, dentists must employ a combination of proactive approaches and reactive solutions to ensure that their practices remain efficient and productive.

Using a simple software program can be an effective way for dentists to streamline their scheduling. Such software programs can allow dentists to easily manage their patient appointments, track patient records, and more. It can also be used to automate the process of appointment reminders, reducing the need for manual follow-ups. Ultimately, utilizing a simple software program can help dentists better manage their scheduling, allowing them to provide a more efficient service to their patients.


\newpage
## Software Development Life Cycle (SDLC)

![SDLC illustration](./sdlc.jpg)

The System Development Life Cycle (SDLC) is a crucial element of any successful software or systems development project. It is the process of planning, designing and developing, testing, and implementing a project. SDLC consists of various phases, each of which is designed to ensure that the project meets the requirements and objectives of the stakeholders and end-users.

The first phase of the SDLC is the planning phase, which is used to determine the scope and objectives of the project. During this phase, stakeholders and end-users are identified and a project plan is created. This plan is used to identify the resources, timeline, and budget needed to complete the project.

The second phase of the SDLC is the analysis phase. During this phase, all project requirements are identified, documented, and analyzed to determine the project's scope, the system boundaries, and the best solution for the problem at hand

The third phase of the SDLC is the design phase. During this phase, the team designs the system based on the project plan. This phase involves creating the architecture for the system, designing the user interface, and developing the database.

The fourht phase of the SDLC is the development phase. During this phase, the team develops the actual software and systems using the design created in the previous phase. This phase also involves testing the system to ensure that it meets the requirements and objectives of the stakeholders and end-users.

The fifth phase of the SDLC is the implementation phase. During this phase, the system is put into production and deployed to the end-users. This phase also includes training users on the system and providing support for the system. Once the system is in production and the users are trained, the project can be considered complete.

The System Development Life Cycle is a complex process that requires a thorough understanding of the project objectives and requirements. By following the SDLC, teams can ensure that projects are completed on time and within budget, and that the system meets the needs of stakeholders and end-users.

### Phase 1: Planning

The Planning Phase is the first phase in the Systems Development Life Cycle (SDLC). It is an essential step in any project, as it provides the framework for the successful completion of the project. The following are the key elements of the Planning Phase:

- Defining the Scope: This involves defining the objectives of the project, outlining the requirements and determining the resources necessary to complete the project. This is done through detailed research and analysis.

- Estimating the Cost: In this stage, the cost of the project is estimated, including the cost of labour, materials, and any other costs associated with the project.

- Developing the Project Plan: A project plan is developed, which outlines the timeline, tasks, deliverables, and resources required to complete the project. This plan serves as a blueprint for the successful completion of the project.

- Assigning Roles and Responsibilities: The project team is organized and roles and responsibilities are assigned to each team member. This ensures that each team member understands their responsibility and can contribute to the successful completion of the project.

By implementing the above steps in the Planning Phase, the project team is able to ensure that the project is properly defined and that the resources and timeline required for the successful completion of the project are established. This leads to a smoother and more successful project.

### Phase 2: Analysis

The Analysis Phase of the System Development Life Cycle (SDLC) is a critical part of the process that determines the success of a project. During this phase, all project requirements are identified, documented, and analyzed to determine the project's scope, the system boundaries, and the best solution for the problem at hand.

The following activities are typically performed during the Analysis Phase:

- Problem analysis: During this activity, the system problem or opportunity is identified, and the stakeholders are interviewed to gain an understanding of the system requirements.

- Feasibility study: A feasibility study is conducted to assess the viability of the project. This includes analyzing the system requirements, technical capabilities, and cost-benefit analysis.

- System requirements analysis: During this activity, the system requirements are documented and analyzed. This includes functional requirements, user requirements, non-functional requirements, and any other relevant requirements.

- Solution design: Once the system requirements are identified and documented, the project team begins designing the solution. This includes designing the architecture, interfaces, and processes required to meet the system requirements.

The Analysis Phase is a critical part of the SDLC and must be performed accurately and thoroughly to ensure the success of the project. This phase is essential for determining the project scope and the best solution for the problem at hand.

### Phase 3: Design

The Design Phase is a critical component of the Systems Development Life Cycle (SDLC). This phase is where the system requirements are turned into a functional design. It is also where the technical solutions for the system are developed and evaluated.

The following are the steps typically involved in the Design Phase:

- Define the Design Requirements: During this step, the system requirements are examined and a detailed design structure is created.

- Evaluate Technical Solutions: During this step, technical solutions are evaluated in terms of their technical feasibility and cost.

- Develop Technical Specifications: Once a technical solution has been chosen, it is necessary to develop a detailed technical specification that outlines the system requirements and the features that must be implemented.

- Develop Test Plans: In order to ensure that the system meets the given requirements, it is necessary to develop a test plan that outlines the test scenarios and test cases that must be executed.

By completing the above steps, the Design Phase serves to ensure that the system is built correctly and that all the required features are implemented. It is through this process that the system is able to move into the testing phase.

### Phase 4: Development

The Development Phase is an important step in the Systems Development Life Cycle (SDLC). It is the phase in which the actual development of the project takes place. During this phase, the project is brought to life, with the development of the functionality, features, and components specified in the Design Phase. The following outlines the key activities and considerations during the Development Phase of a project.

- Establishing the Development Environment: The development environment is the environment in which the project is developed. This may include setting up the appropriate hardware, software, and any other necessary elements. This step also typically includes creating the database, setting up the version control system, and establishing the project coding standards.

- Developing the Project: This step involves developing the project according to the specifications outlined in the Design Phase. This step typically involves coding the project, testing it, and debugging any errors.

- Integrating the Components: This step involves integrating all of the different components of the project into a cohesive system. This may include creating the necessary interfaces between components, as well as any other necessary integration steps.

- Testing and Debugging: This step involves testing the project to ensure that it meets the requirements outlined in the Design Phase. This includes testing for performance, reliability, and usability. Any errors found during this step should be documented and corrected as necessary.

In summary, the Development Phase is an important step in the Systems Development Life Cycle. This is the phase in which the project is actually developed, with the necessary hardware, software, coding, and testing taking place. It is important to ensure that all of the components are properly integrated and tested to ensure the project meets its requirements.

#### Phase 5: Implementation

The Implementation Phase is a pivotal stage of the System Development Life Cycle (SDLC) and is the stage where the project is brought to life. During this phase, the developed system is implemented, tested, and deployed into the production environment. The following points outline the various activities involved in the Implementation Phase of the SDLC:

* Configuration and installation: All components of the system must be configured and installed in the production environment. The necessary hardware, software, and network infrastructure must be set up in order to ensure the system is properly functioning.

* Testing and validation: Once the system is installed, it must undergo thorough testing to ensure the system meets the requirements of the development project. This may involve user acceptance testing to ensure the system performs to the satisfaction of the customers.

* Deployment: After the system has passed thorough testing and validation, it can be deployed into the production environment. This may involve the setup of the necessary security protocols and the provisioning of the necessary resources.

* Maintenance: Once the system is deployed, it must be maintained on a regular basis. This may include the implementation of patches and updates, as well as making sure the system is running optimally. Regular maintenance can help to ensure the system is secure and up to date.


### Phase 6: Support

The Support Phase of the SDLC is a critical milestone in the successful delivery of a project. It is the final stage of the system development life cycle and ensures that the system meets the needs of the stakeholders. In this phase, the system is tested and deployed to production and the team works to ensure that the system functions correctly and meets the expectations of the stakeholders.

The Support Phase is an important part of the system development life cycle as it ensures that the system is stable and reliable before it goes into production. During this phase, the team will identify and fix any errors or bugs that may be present in the system. Additionally, they will monitor the system performance and make any necessary changes to ensure that the system is functioning as expected.

The Support Phase can include the following activities:

* Testing and validation of the system

* Monitoring system performance

* Making any necessary changes to ensure that the system is functioning as expected

* Identifying and resolving any issues

* Collecting feedback from stakeholders

The Support Phase is a necessary step in the successful delivery of a project. It ensures that the system is stable and reliable before it goes into production. By ensuring that the system meets the expectations of the stakeholders, the Support Phase can help to ensure that the system is successful and that the project is successful.

\newpage

# Software Testing
Software testing is an important process in the development of any software application or product. It helps ensure that the software is functioning correctly and meets the necessary requirements of its users. This article will provide an overview of software testing, including what it is, why it is important, and the different types of software testing available. Additionally, it will provide an in-depth explanation of two of the most popular types of software testing: black box and white box testing, as well as their advantages and disadvantages.

## Software Testing
Software testing is the process of evaluating a software application or product to determine its correctness, completeness, and quality. It is done to ensure that the software meets all the requirements of its users and performs as expected without any flaws. It involves testing the software against different types of scenarios and environments, and testing various aspects of the software, such as functionality, usability, reliability, security, and performance.

Software testing is important for a number of reasons. It helps to identify any flaws or weaknesses in the software, which can then be addressed before the software is released to the public. This helps to ensure that the software performs as expected and meets the requirements of its users. Additionally, software testing helps to reduce the risk of software failure and improves the overall quality of the software.

Two of the most popular types of software testing are black box and white box testing.

### Black Box Testing
Black box testing is a type of software testing that is performed without any knowledge of the internal workings of the software. It tests the functionality of the software, based on its external specifications. It is used to test the functionality of the software, without having to worry about the internal structure of the software.

**Advantages:**
* Faster testing time, as knowledge of the internal structure of the software is not necessary.
* Easy to use, as tests do not need to be designed with knowledge of the internal structure of the software.

**Disadvantages:**
* Tests can be limited, as the tester has no knowledge of the internal structure of the software.
* Can be difficult to identify the cause of bugs, as the tester does not have any knowledge of the internal structure of the software.

### White Box Testing
White box testing is a type of software testing that is performed with knowledge of the internal workings of the software. It tests the internal structure of the software, to ensure that it is functioning correctly. It is used to test the functionality of the software, as well as the internal structure of the software.

**Advantages:**
* Can test more aspects of the software, as knowledge of the internal structure is required.
* Easier to identify the cause of bugs, as the tester has knowledge of the internal structure of the software.

**Disadvantages:**
* Can be time-consuming, as knowledge of the internal structure of the software is necessary.
* Tests can be difficult to design, as knowledge of the internal structure of the software is necessary.

## History of Python

Python is an interpreted, high-level, general-purpose programming language that was created in the late 1980s by Guido van Rossum.

### Early Development

Python's development began in the late 1980s, when Guido van Rossum began work on the language at the National Research Institute for Mathematics and Computer Science (NRIMC) in the Netherlands. The language was initially intended to be a successor to the ABC language, which was developed by van Rossum during his time at the institute.

Van Rossum then moved to the United States and worked at the Corporation for National Research Initiatives (CNRI), where he further developed the language and released its first version in 1991.

### Growth and Adoption

Python quickly gained popularity among developers and became widely adopted in the software development community. In 2000, van Rossum and the Python Software Foundation released version 2.0 of the language, which further increased its popularity.

Python has become one of the most popular programming languages in the world, with its use in software development, data science, machine learning, artificial intelligence, and web development.

### Key Features

Python is a versatile language that is designed to be easy to read and write. It has a simple syntax that makes it easy to learn, and it supports a wide range of programming paradigms, including object-oriented, procedural, and functional programming.

Python also supports multiple platforms and operating systems, making it a highly portable language. It also has a large standard library that includes modules for a variety of tasks, such as networking, database access, web development, and more.

### Conclusion

Python has come a long way since its initial release in 1991. It has become one of the most popular programming languages in the world, and its growth shows no signs of slowing down. Python is a powerful language that is designed to be easy to learn and use, and its wide range of features make it a great choice for a variety of projects.

\newpage

## History of MySQL

MySQL is an open-source relational database management system (RDBMS) first released in 1995. It is developed and maintained by Oracle Corporation, and is one of the most popular database systems in the world.

### Origins

MySQL was created by Swedish software engineer Michael Widenius and David Axmark in 1994. The project began as a low-level language interface to the popular mSQL database system. They developed the system further, and released the first version of MySQL in 1995.

### Growth

MySQL quickly gained popularity due to its open-source license and ease of use. It gained even greater traction when Sun Microsystems purchased the company in 2008 and open-sourced the database system. MySQL is now used by a wide variety of companies, from small businesses to large corporations.

### Recent Developments

In 2010, Oracle purchased Sun Microsystems and took ownership of MySQL. Oracle has continued to develop and maintain the system, and has made several changes to the system in recent years, including introducing new features like stored procedures and triggers.

MySQL remains one of the most popular database systems in the world, and is commonly used in web development, data warehousing, and other applications. It is an important part of many companies' infrastructure, and is an integral part of the open-source software ecosystem.

\newpage

## Introduction to Python-MySQL Connectivity

Python and MySQL are two of the most popular and widely used technologies for data storage and processing. By connecting them, developers can create powerful applications that can store and manage data in an efficient manner. Python-MySQL connectivity enables developers to work with data stored in MySQL databases using the powerful features of Python.

### Benefits of Using Python-MySQL Connectivity

Python-MySQL connectivity offers numerous benefits to developers, such as:

- **Flexibility**: Python-MySQL connectivity allows developers to create applications that are highly flexible and can easily scale with data growth.

- **Ease of Access**: Connecting Python and MySQL enables developers to access data stored in MySQL databases with ease.

- **Reliability**: Python-MySQL connectivity ensures that data is secure and reliable for long-term storage.

- **Performance**: By connecting Python and MySQL, developers can create applications with improved performance and speed.

These advantages make Python-MySQL connectivity an attractive option for developers looking to build powerful applications for data storage and processing.

### Connectivity
Python and MySQL are connected using the `mysql` package.

\newpage

# Source Code

## SQL queries to create DB and table

~~~~~{.sql}
CREATE DATABASE IF NOT EXISTS dentist_appointments;
USE dentist_appointments;
CREATE TABLE IF NOT EXISTS appointments (
    id INT AUTO_INCREMENT PRIMARY KEY NOT NULL,
    name VARCHAR(255) NOT NULL,
    email VARCHAR(255) NOT NULL,
    date DATE NOT NULL,
    message VARCHAR(255)

);
~~~~~

## File: `main.py`

~~~~~{.python .numberLines}
# License: MIT

import mysql.connector
from datetime import datetime, timedelta
from sys import stderr

# Connect to MySQL database
mydb = mysql.connector.connect(
  host="localhost",
  user="root",
  password="",
  database="dentist_appointments"
)

# Create a cursor object to interact with the database
cursor = mydb.cursor()

def display_appointments():
    # Fetch all records from the appointments table
    cursor.execute("SELECT * FROM appointments ORDER BY date DESC")
    appointments = cursor.fetchall()

    # Display the appointments
    print("\n------------------------")
    for appointment in appointments:
        # print(f"Id: {appointment[0]}")
        print(f"Name: {appointment[1]}")
        print(f"Email: {appointment[2]}")
        print(f"Date: {appointment[3]}")
        if appointment[4]:
            print(f"Message: {appointment[4]}")
        print("------------------------")

def book_appointment():
    # Prompt the user for their name, email, and date of appointment
    print("\n------------------------")
    name = input("Enter your name: ")
    email = input("Enter your email address: ")
    date = input("Enter the date of appointment (YYYY-MM-DD): ")
    Message = input("Enter a short message for the doctor (optional): ")
    print("------------------------\n")

    try:
        # Insert the appointment into the database
        sql = "INSERT INTO appointments (name, email, date, Message) VALUES (%s, %s, %s, %s)"
        values = (name, email, date, Message)
        cursor.execute(sql, values)
        mydb.commit()
    except:
        print("ERROR: Error while updating database. Please check your input and try again.", file=stderr)
        return

    print("Appointment booked successfully!\n")

def view_due_appointments():
    # Calculate the date six months ago
    six_months_ago = datetime.now() - timedelta(days=180)

    # Fetch appointments older than
    sql = "SELECT * FROM appointments WHERE date <= %s"
    values = (six_months_ago,)
    cursor.execute(sql, values)
    appointments = cursor.fetchall()

    # Display the due appointments
    print("\nThese appointments are older than six months. Consider notifying these patients for a new appointment.")
    print("------------------------")
    for appointment in appointments:
        print(f"Name: {appointment[0]}")
        print(f"Email: {appointment[1]}")
        print(f"Date: {appointment[2]}")
        print("------------------------")

# Main program flow
while True:
    print("\nWelcome to the Dentist Appointment Scheduler!")
    print("Please select an option:")
    print("1. Book an appointment")
    print("2. View all appointments")
    print("3. View due appointments (older than six months)")
    print("4: Exit")
    choice = input("Enter your choice: ")

    if choice == "1":
        book_appointment()
    elif choice == "2":
        display_appointments()
    elif choice == "3":
        view_due_appointments()
    elif choice == "4":
        break
    else:
        print("Invalid choice. Please try again.", file=stderr)

    print("\n")

print("Terminating program...")
~~~~~~

\newpage

# Output and Demonstration

## 1. Creating a new appointment
![](./create.png)

## 2. Viewing all appointments
![](./view-all.png)

## 3. Viewing due appointments
![](./view-due.png)

## 4. Database query output
![](./table.png)

\newpage

# System Requirements

This should run on any reasonably modern system which supports Python 3.x and MySQL 5.x or 8.x, including embedded systems and single-board computers.

As a rule of thumb, the following specifications are recommended:

- 128 MB of RAM
- 512 MB of storage
- A CPU with at least one core
- A *proper* operating system, preferably POSIX-compliant. Windows may work but it is an inferior system in nearly every aspect and is hence not recommended.

# Software Dependencies

- A decent operating system, like Fedora Linux. Other proprietary operating systems work but they're not recommended
- A Python installation with `pip`
- A MySQL or MariaDB installation
- A terminal emulator or an equivalent program with `stdin/stdout`
- The pip package `mysql.connnector` (Can be installed using `$ pip install mysql.connector`)

\newpage

# Credits and Bibliography

- The official Python documentation (<https://docs.python.org>)
  
- MySQL Connector/Python documentation (<https://dev.mysql.com/doc/connector-python/en/>)
  
- MariaDB (Libre MySQL fork, not owned by Oracle Inc.) documentation (<https://mariadb.com/docs/>)

## Software Used:

#### A. Development

- Text editor: VSCodium - A libre fork of Visual Studio Code (<https://vscodium.com>)

- Python binaries from Fedora repos

- Terminal: Kitty (<https://sw.kovidgoyal.net/kitty>)

- Operating System: Fedora Linux (<https://getfedora.org>)

- Environment: The GNOME Desktop Environment (<https://gnome.org>) 

#### B. Report Writing

- Written in the Markdown format using Marker 
- Typeset in \LaTeX
- Format conversion using `pandoc`